import 'package:flutter/material.dart';
import 'dart:math';

void main() => runApp(
      MaterialApp(
        home: Magic8Ball(),
      ),
    );

class Magic8Ball extends StatefulWidget {
  @override
  _Magic8BallState createState() => _Magic8BallState();
}

class _Magic8BallState extends State<Magic8Ball> {
  var rng = new Random();
  int answerNumber = 5;

  void getAnswer() {
    setState(() {
      answerNumber = rng.nextInt(5) + 1;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.lightBlue[400],
      appBar: AppBar(
        backgroundColor: Colors.blue[700],
        title: Text('Ask me anything'),
      ),
      body: Column(
        children: [
          Expanded(
            child: FlatButton(
              onPressed: getAnswer,
              child: Image(
                image: AssetImage('images/ball$answerNumber.png'),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
